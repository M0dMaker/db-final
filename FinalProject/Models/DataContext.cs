﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// Manages a connection with the SQL database and performs queries.
    /// </summary>
    public sealed class DataContext : IDisposable
    {
        /*CREATE TABLE [dbo].[Author] (
            [Id]       INT           IDENTITY (1, 1) NOT NULL,
            [Name]     VARCHAR (MAX) NOT NULL,
            [Email]    VARCHAR (MAX) NOT NULL,
            [JoinDate] DATETIME      NOT NULL,
            PRIMARY KEY CLUSTERED ([Id] ASC)
        );
        CREATE TABLE [dbo].[Post] (
            [Id]       INT           IDENTITY (1, 1) NOT NULL,
            [Name]     VARCHAR (MAX) NOT NULL,
            [Text]     TEXT          NOT NULL,
            [Date]     DATETIME      NOT NULL,
            [Category] VARCHAR (MAX) NOT NULL,
            [Author]   INT           NOT NULL,
            PRIMARY KEY CLUSTERED ([Id] ASC),
            FOREIGN KEY ([Author]) REFERENCES [dbo].[Author] ([Id])
        );
        CREATE TABLE [dbo].[Comment] (
            [Id]   INT           IDENTITY (1, 1) NOT NULL,
            [User] VARCHAR (MAX) NOT NULL,
            [Post] INT           NOT NULL,
            [Text] TEXT          NOT NULL,
            [Date] DATETIME      NOT NULL,
            PRIMARY KEY CLUSTERED ([Id] ASC),
            FOREIGN KEY ([Post]) REFERENCES [dbo].[Post] ([Id])
        );
        CREATE TABLE [dbo].[PostTag] (
            [PostId] INT           NOT NULL,
            [Name]   VARCHAR (255) NOT NULL,
            PRIMARY KEY CLUSTERED ([PostId] ASC, [Name] ASC),
            FOREIGN KEY ([PostId]) REFERENCES [dbo].[Post] ([Id])
        );
        CREATE TABLE [dbo].[Link] (
            [Id]   INT           IDENTITY (1, 1) NOT NULL,
            [Text] VARCHAR (MAX) NOT NULL,
            [Href] VARCHAR (MAX) NOT NULL,
            PRIMARY KEY CLUSTERED ([Id] ASC)
        );*/

        /// <summary>
        /// The connection string used to connect to the database.
        /// </summary>
        private static string ConnectionString = ConfigurationManager
                                                    .ConnectionStrings["DefaultConnection"]
                                                    .ConnectionString;

        /// <summary>
        /// The current connection to the database.
        /// </summary>
        private SqlConnection con;
        /// <summary>
        /// Whether the object is disposed.
        /// </summary>
        private bool _disposed = false;
        
        /// <summary>
        /// Creates a new DataContext object and connects to the database.
        /// </summary>
        public DataContext()
        {
            con = new SqlConnection(ConnectionString);
            con.Open();
        }
        /// <summary>
        /// Destroys the object and closes the connection.
        /// </summary>
        ~DataContext()
        {
            Dispose(false);
        }

        /// <summary>
        /// The current authors in the database.
        /// </summary>
        public Author[] Authors
        {
            get
            {
                using (var command = Command(
                    @"SELECT *, 
                          (SELECT COUNT(*) FROM [dbo].[Post] p WHERE p.[Author] = a.[Id]) as [Count]
                          FROM [dbo].[Author] a"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<Author>();
                        while (reader.Read())
                        {
                            ret.Add(new Author(reader));
                        }

                        return ret.ToArray();
                    }
                }
            }
        }
        /// <summary>
        /// Updates the data for the given author.
        /// </summary>
        /// <param name="author">The author to update.</param>
        public void UpdateAuthor(Author author)
        {
            SqlCommand command;
            if (author.Id == -1)
            {
                command = Command(
                    @"INSERT INTO [dbo].[Author] ([Name], [Email], [JoinDate])
                        VALUES (@name, @email, @join)");
            }
            else
            {
                command = Command(
                    @"UPDATE [dbo].[Author]
                        SET [Name]=@name, [Email]=@email, [JoinDate]=@join
                        WHERE [Id]=@id");
                command.Parameters.AddWithValue("@id", author.Id);
            }

            command.Parameters.AddWithValue("@name", author.Name);
            command.Parameters.AddWithValue("@email", author.Email);
            command.Parameters.AddWithValue("@join", author.JoinDate.ToString());

            command.ExecuteNonQuery();
            command.Dispose();
        }
        /// <summary>
        /// Deletes the author with the given id.
        /// </summary>
        /// <param name="id">The id of the author to delete.</param>
        public void DeleteAuthor(int id)
        {
            using (var command = Command("DELETE FROM [dbo].[Author] WHERE [Id]=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// The current comments in the database.
        /// </summary>
        public Comment[] Comments
        {
            get
            {
                using (var command = Command(
                    @"SELECT [Id], [Text], [Date], [User],
                          (SELECT [Name] FROM [dbo].[Post] p WHERE p.[Id] = c.[Post]) as [Post]
                          FROM [dbo].[Comment] c"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<Comment>();
                        while (reader.Read())
                        {
                            ret.Add(new Comment(reader));
                        }

                        return ret.ToArray();
                    }
                }
            }
        }
        /// <summary>
        /// The names of the posts in the database.
        /// </summary>
        public string[] PostNames
        {
            get
            {
                using (var command = Command(
                    @"SELECT [Name] FROM [dbo].[Post]"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<string>();
                        while (reader.Read())
                        {
                            ret.Add((string)reader["Name"]);
                        }

                        return ret.ToArray();
                    }
                }
            }
        }
        /// <summary>
        /// Updates the data for the given comment.
        /// </summary>
        /// <param name="comment">The comment to update.</param>
        public void UpdateComment(Comment comment)
        {
            SqlCommand command;
            if (comment.Id == -1)
            {
                command = Command(
                    @"INSERT INTO [dbo].[Comment] ([User], [Text], [Date], [Post])
                        VALUES (@user, @text, @date,
                            (SELECT [Id] FROM [dbo].[Post] WHERE [Name]=@post))");

                command.Parameters.AddWithValue("@user", comment.User);
                command.Parameters.AddWithValue("@post", comment.Post);
            }
            else
            {
                command = Command(
                    @"UPDATE [dbo].[Comment]
                        SET [Text]=@text, [Date]=@date
                        WHERE [Id]=@id");
                command.Parameters.AddWithValue("@id", comment.Id);
            }

            command.Parameters.AddWithValue("@text", comment.Text);
            command.Parameters.AddWithValue("@date", comment.Date.ToString());

            command.ExecuteNonQuery();
            command.Dispose();
        }
        /// <summary>
        /// Deletes the comment with the given id.
        /// </summary>
        /// <param name="id">The id of the comment to delete.</param>
        public void DeleteComment(int id)
        {
            using (var command = Command("DELETE FROM [dbo].[Comment] WHERE [Id]=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// The current posts in the database.
        /// </summary>
        public Post[] Posts
        {
            get
            {
                using (var command = Command(
                    @"SELECT [Id], [Name], [Text], [Date], [Category],
                        (SELECT [Name] FROM [dbo].[Author] a WHERE a.[Id] = p.[Author]) as [Author],
                        (SELECT Count(*) FROM [dbo].[Comment] c WHERE c.[Post] = p.[Id]) as [Count]
                        FROM [dbo].[Post] p"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<Post>();
                        while (reader.Read())
                        {
                            ret.Add(new Post(reader));
                        }

                        return ret.ToArray();
                    }
                }
            }
        }
        /// <summary>
        /// The names of the authors in the database.
        /// </summary>
        public string[] AuthorNames
        {
            get
            {
                using (var command = Command(
                    @"SELECT [Name] FROM [dbo].[Author]"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<string>();
                        while (reader.Read())
                        {
                            ret.Add((string)reader["Name"]);
                        }

                        return ret.ToArray();
                    }
                }
            }
        }
        /// <summary>
        /// Gets all the posts in the given year, and the given month if given.
        /// </summary>
        /// <param name="year">The year to get the posts in.</param>
        /// <param name="month">The month to get the posts in; or null to get for the year.</param>
        /// <returns>The posts in the given year and month.</returns>
        public Post[] GetPosts(int year, int? month)
        {
            using (var command = Command(
                @"SELECT [Id], [Name], [Text], [Date], [Category],
                        (SELECT [Name] FROM [dbo].[Author] a WHERE a.[Id] = p.[Author]) as [Author],
                        (SELECT Count(*) FROM [dbo].[Comment] c WHERE c.[Post] = p.[Id]) as [Count]
                        FROM [dbo].[Post] p
                        WHERE YEAR([Date]) = @year" + 
                        (month.HasValue ? " AND MONTH([Date]) = @month" : "")))
            {
                command.Parameters.AddWithValue("@year", year);
                if (month.HasValue)
                    command.Parameters.AddWithValue("@month", month.Value);
                using (var reader = command.ExecuteReader())
                {
                    var ret = new List<Post>();
                    while (reader.Read())
                    {
                        ret.Add(new Post(reader));
                    }

                    return ret.ToArray();
                }
            }

        }
        /// <summary>
        /// Gets the comments for the post with the given id.
        /// </summary>
        /// <param name="id">The id of the post.</param>
        /// <returns>The comments for the given post.</returns>
        public Comment[] GetComments(int id)
        {
            using (var command = Command(
                @"SELECT [Id], [Text], [Date], [User],
                        (SELECT [Name] FROM [dbo].[Post] p WHERE p.[Id] = c.[Post]) as [Post]
                        FROM [dbo].[Comment] c
                        WHERE c.[Post] = @post"))
            {
                command.Parameters.AddWithValue("@post", id);

                using (var reader = command.ExecuteReader())
                {
                    var ret = new List<Comment>();
                    while (reader.Read())
                    {
                        ret.Add(new Comment(reader));
                    }

                    return ret.ToArray();
                }
            }
        }
        /// <summary>
        /// Updates the data for the given post.
        /// </summary>
        /// <param name="post">The post to update.</param>
        public void UpdatePost(Post post)
        {
            SqlCommand command;
            if (post.Id == -1)
            {
                command = Command(
                    @"INSERT INTO [dbo].[Post] ([Name], [Text], [Date], [Category], [Author])
                        VALUES (@name, @text, @date, @category, 
                            (SELECT [Id] FROM [dbo].[Author] WHERE [Name]=@author))");

                command.Parameters.AddWithValue("@author", post.Author);
            }
            else
            {
                command = Command(
                    @"UPDATE [dbo].[Post]
                        SET [Text]=@text, [Date]=@date, [Category]=@category, [Name]=@name
                        WHERE [Id]=@id");
                command.Parameters.AddWithValue("@id", post.Id);
            }

            command.Parameters.AddWithValue("@name", post.Title);
            command.Parameters.AddWithValue("@category", post.Category);
            command.Parameters.AddWithValue("@text", post.Text);
            command.Parameters.AddWithValue("@date", post.Date.ToString());

            command.ExecuteNonQuery();
            command.Dispose();
        }
        /// <summary>
        /// Deletes the post with the given id.  This also deletes
        /// the comments for this post.
        /// </summary>
        /// <param name="id">The id of the post to delete.</param>
        public void DeletePost(int id)
        {
            using (var tran = con.BeginTransaction(System.Data.IsolationLevel.Serializable))
            {
                using (var command = Command("DELETE FROM [dbo].[PostTag] WHERE [PostId]=@id"))
                {
                    command.Transaction = tran;
                    command.Parameters.AddWithValue("@id", id);
                    command.ExecuteNonQuery();
                }
                using (var command = Command("DELETE FROM [dbo].[Comment] WHERE [Post]=@id"))
                {
                    command.Transaction = tran;
                    command.Parameters.AddWithValue("@id", id);
                    command.ExecuteNonQuery();
                }
                using (var command = Command("DELETE FROM [dbo].[Post] WHERE [Id]=@id"))
                {
                    command.Transaction = tran;
                    command.Parameters.AddWithValue("@id", id);
                    command.ExecuteNonQuery();
                }

                tran.Commit();
            }
        }

        /// <summary>
        /// The current links in the database.
        /// </summary>
        public Link[] Links
        {
            get
            {
                using (var command = Command(
                    @"SELECT * FROM [dbo].[Link] p"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<Link>();
                        while (reader.Read())
                        {
                            ret.Add(new Link(reader));
                        }

                        return ret.ToArray();
                    }
                }
            }
        }
        /// <summary>
        /// Updates the data for the given link.
        /// </summary>
        /// <param name="link">The link to update.</param>
        public void UpdateLink(Link link)
        {
            SqlCommand command;
            if (link.Id == -1)
            {
                command = Command(
                    @"INSERT INTO [dbo].[Link] ([Text], [Href])
                        VALUES (@text, @href)");
            }
            else
            {
                command = Command(
                    @"UPDATE [dbo].[Link]
                        SET [Text]=@text, [Href]=@href
                        WHERE [Id]=@id");
                command.Parameters.AddWithValue("@id", link.Id);
            }

            command.Parameters.AddWithValue("@text", link.Text);
            command.Parameters.AddWithValue("@href", link.Href);

            command.ExecuteNonQuery();
            command.Dispose();
        }
        /// <summary>
        /// Deletes the link with the given id.
        /// </summary>
        /// <param name="id">The id of the link to delete.</param>
        public void DeleteLink(int id)
        {
            using (var command = Command("DELETE FROM [dbo].[Link] WHERE [Id]=@id"))
            {
                command.Parameters.AddWithValue("@id", id);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Gets the current archive entries for the database.
        /// </summary>
        public MasterModel.ArchiveEntry[] Archives
        {
            get
            {
                using (var command = Command(
                    @"SELECT TOP 5 YEAR(p.[Date]) as [Year], MONTH(p.[Date]) as [Month], COUNT(*) as [Count] 
                      FROM [dbo].[Post] p
                      GROUP BY YEAR(p.[Date]), MONTH(p.[Date])
                      ORDER BY YEAR(p.[Date]) DESC, MONTH(p.[Date]) DESC"))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var ret = new List<MasterModel.ArchiveEntry>();
                        while (reader.Read())
                        {
                            ret.Add(new MasterModel.ArchiveEntry(reader));
                        }

                        return ret.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new command with the given text.  Does not execute it.
        /// </summary>
        /// <param name="command">The text for the command.</param>
        /// <returns>A new command for the given text.</returns>
        SqlCommand Command(string command)
        {
            var ret = con.CreateCommand();
            ret.CommandText = command;
            return ret;
        }

        /// <summary>
        /// Disposes the current object and closes the connection.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                GC.SuppressFinalize(this);
                Dispose(true);
            }
        }
        /// <summary>
        /// Disposes the current object and closes the connection.
        /// </summary>
        /// <param name="disposing">Whether to close the connection.</param>
        void Dispose(bool disposing)
        {
            if (disposing)
            {
                con.Dispose();
            }

            con = null;
        }
    }
}