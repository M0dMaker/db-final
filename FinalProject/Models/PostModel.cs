﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// The model class for the PostAdmin/Single view.
    /// </summary>
    public sealed class PostModel
    {
        /// <summary>
        /// The post that is being edited.
        /// </summary>
        public Post Post;
        /// <summary>
        /// The possible author names.
        /// </summary>
        public string[] Authors;
        /// <summary>
        /// Contains the comments for the post.
        /// </summary>
        public Comment[] Comments;

        /// <summary>
        /// Creates a new PostModel with the given data.
        /// </summary>
        /// <param name="post">The post to edit.</param>
        /// <param name="comments">The comments for the post.</param>
        /// <param name="authors">The possible author names.</param>
        public PostModel(Post post, Comment[] comments, string[] authors)
        {
            Comments = comments;
            Post = post;
            Authors = authors;
        }
    }
}