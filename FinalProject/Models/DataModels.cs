﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// Defines an author for use in the database.
    /// </summary>
    public sealed class Author
    {
        /// <summary>
        /// Contains the ID of the author, this cannot be changed.
        /// </summary>
        public readonly int Id;
        /// <summary>
        /// Whether the author can be deleted.
        /// </summary>
        public readonly bool CanDelete;
        /// <summary>
        /// Contains the name of the author.
        /// </summary>
        public string Name;
        /// <summary>
        /// Contains the email of the author.
        /// </summary>
        public string Email;
        /// <summary>
        /// Contains the date and time that the author joined.
        /// </summary>
        public DateTime JoinDate;

        /// <summary>
        /// Creates a new author from the given SQL data.
        /// </summary>
        /// <param name="data">The data reader to read data from.</param>
        public Author(SqlDataReader data)
        {
            Id = (int)data["Id"];
            Name = (string)data["Name"];
            Email = (string)data["Email"];
            JoinDate = (DateTime)data["JoinDate"];
            CanDelete = (int)data["Count"] == 0;
        }
        /// <summary>
        /// Creates a new author object.
        /// </summary>
        public Author()
        {
            Id = -1;
            JoinDate = DateTime.Now;
            CanDelete = false;
        }
    }
    /// <summary>
    /// Defines a post for use in the database.
    /// </summary>
    public sealed class Post
    {
        /// <summary>
        /// Contains the ID of the post, this cannot be changed.
        /// </summary>
        public readonly int Id;
        /// <summary>
        /// Contains the author of the post.
        /// </summary>
        public string Author;
        /// <summary>
        /// The title of the post.
        /// </summary>
        public string Title;
        /// <summary>
        /// Contains the text of the post.
        /// </summary>
        public string Text;
        /// <summary>
        /// Contains the date and time the post was made.
        /// </summary>
        public DateTime Date;
        /// <summary>
        /// Contains the category of the post.
        /// </summary>
        public string Category;
        /// <summary>
        /// Contains the string for the comments.
        /// </summary>
        public string Comments;

        /// <summary>
        /// Creates a new post object.
        /// </summary>
        public Post()
        {
            Id = -1;
            Date = DateTime.Now;
            Comments = "Leave Comment";
        }
        /// <summary>
        /// Creates a new post from the given SQL data.
        /// </summary>
        /// <param name="reader">The SQL data reader.</param>
        public Post(SqlDataReader reader)
        {
            Id = (int)reader["Id"];
            Author = (string)reader["Author"];
            Text = (string)reader["Text"];
            Title = (string)reader["Name"];
            Date = (DateTime)reader["Date"];
            Category = (string)reader["Category"];

            int count = (int)reader["Count"];
            if (count < 1)
                Comments = "Leave Comment";
            else if (count == 1)
                Comments = "1 Comment";
            else
                Comments = string.Format("{0} Comments", count);
        }
    }
    /// <summary>
    /// Defines a comment for use in the database.
    /// </summary>
    public sealed class Comment
    {
        /// <summary>
        /// Contains the ID of the comment, this cannot be changed.
        /// </summary>
        public readonly int Id;
        /// <summary>
        /// Contains the author of the comment.
        /// </summary>
        public string User;
        /// <summary>
        /// Contains the post of the comment.
        /// </summary>
        public string Post;
        /// <summary>
        /// Contains the text of the comment.
        /// </summary>
        public string Text;
        /// <summary>
        /// Contains the date and time the comment was made.
        /// </summary>
        public DateTime Date;

        /// <summary>
        /// Creates a new comment object.
        /// </summary>
        public Comment()
        {
            Id = -1;
            Date = DateTime.Now;
        }
        /// <summary>
        /// Creates a new comment from the given SQL data.
        /// </summary>
        /// <param name="reader">The SQL data reader.</param>
        public Comment(SqlDataReader reader)
        {
            Id = (int)reader["Id"];
            User = (string)reader["User"];
            Post = (string)reader["Post"];
            Text = (string)reader["Text"];
            Date = (DateTime)reader["Date"];
        }
    }
    /// <summary>
    /// Defines a link for use in the database.
    /// </summary>
    public sealed class Link
    {
        /// <summary>
        /// The ID of the link.
        /// </summary>
        public readonly int Id;
        /// <summary>
        /// Contains the text of the link.
        /// </summary>
        public string Text;
        /// <summary>
        /// Contains the url for the link.
        /// </summary>
        public string Href;

        /// <summary>
        /// Creates a new Link object.
        /// </summary>
        public Link()
        {
            Id = -1;
        }
        /// <summary>
        /// Creates a new Link from the given SQl data.
        /// </summary>
        /// <param name="reader">The SQL data reader.</param>
        public Link(SqlDataReader reader)
        {
            Id = (int)reader["Id"];
            Text = (string)reader["Text"];
            Href = (string)reader["Href"];
        }
    }
}