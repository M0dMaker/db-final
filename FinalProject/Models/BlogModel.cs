﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// The model used to give data to the Blog/Index view.
    /// </summary>
    public sealed class BlogIndexModel : MasterModel
    {
        /// <summary>
        /// Contains the year of the archive.
        /// </summary>
        public int? Year;
        /// <summary>
        /// Contains the month of the archive.
        /// </summary>
        public string Month;
        /// <summary>
        /// Contains the numerical value of the month.
        /// </summary>
        public int? RawMonth;
        /// <summary>
        /// Contains the posts to list.
        /// </summary>
        public Post[] Posts;
        /// <summary>
        /// Contains the offset for the next link; negative if there is no next.
        /// </summary>
        public int NextOffset;
        /// <summary>
        /// Contains the offset for the previous link; negative if there is no previous.
        /// </summary>
        public int PrevOffset;

        /// <summary>
        /// Creates a new BlogIndexModel from the given data.
        /// </summary>
        /// <param name="posts">The posts to display.</param>
        /// <param name="offset">The offset in the list.</param>
        /// <param name="year">The year of the archive.</param>
        /// <param name="month">The month of the archive.</param>
        public BlogIndexModel(Post[] posts, int offset, int? year, int? month)
        {
            int count = posts.Count();

            this.Year = year;
            this.RawMonth = month;
            this.Month = month.HasValue ? ((Months)month.Value).ToString() : null;
            this.NextOffset = (offset + 5 < count ? offset + 5 : -1);
            this.PrevOffset = (offset > 0 ? Math.Max(0, offset - 5) : -1);
            this.Posts = posts.OrderByDescending(p => p.Date).Skip(offset).Take(5).ToArray();
        }
    }

    /// <summary>
    /// The model used to give data to the Blog/Single view.
    /// </summary>
    public sealed class BlogSingleModel : MasterModel
    {
        /// <summary>
        /// Contains the post being displayed.
        /// </summary>
        public Post Post;
        /// <summary>
        /// Contains the comments for the post.
        /// </summary>
        public Comment[] Comments;

        /// <summary>
        /// Creates a new BlogSingleModel from the given data.
        /// </summary>
        /// <param name="post">The post to display.</param>
        /// <param name="comments">The comments for the post.</param>
        public BlogSingleModel(Post post, Comment[] comments)
        {
            Post = post;
            Comments = comments;
        }
    }
}