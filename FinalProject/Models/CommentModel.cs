﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// Contains data to given to the CommentAdmin/Single view.
    /// </summary>
    public sealed class CommentModel
    {
        /// <summary>
        /// Contains the comment that is being edited.
        /// </summary>
        public Comment Comment;
        /// <summary>
        /// Contains the possible post names.
        /// </summary>
        public string[] Posts;

        /// <summary>
        /// Creates a new CommentModel object from the given data.
        /// </summary>
        /// <param name="comment">The comment to edit.</param>
        /// <param name="posts">The possible post names.</param>
        public CommentModel(Comment comment, string[] posts)
        {
            Comment = comment;
            Posts = posts;
        }
    }
}