﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// Defines the possible month values.
    /// </summary>
    public enum Months : int
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

    /// <summary>
    /// Defines the base class for models in the main site, the
    /// Admin pages do not use this.
    /// </summary>
    public class MasterModel
    {
        /// <summary>
        /// Defines an entry in the archive sidebar.
        /// </summary>
        public class ArchiveEntry
        {
            /// <summary>
            /// Contains the integer value of the month.
            /// </summary>
            public int Month;
            /// <summary>
            /// Contains the integer value of the year.
            /// </summary>
            public int Year;
            /// <summary>
            /// Contains the number of entries in the month.
            /// </summary>
            public int Count;

            /// <summary>
            /// Creates a new ArchiveEntry object.
            /// </summary>
            /// <param name="month">The month of the archive.</param>
            /// <param name="year">The year of the archive.</param>
            /// <param name="count">The number of entries in this month.</param>
            public ArchiveEntry(int month, int year, int count)
            {
                this.Month = month;
                this.Year = year;
                this.Count = count;
            }
            /// <summary>
            /// Creates a new ArchiveEntry from the given SQL data.
            /// </summary>
            /// <param name="reader">The SQL data reader.</param>
            public ArchiveEntry(SqlDataReader reader)
            {
                Month = (int)reader["Month"];
                Year = (int)reader["Year"];
                Count = (int)reader["Count"];
            }
        }

        /// <summary>
        /// Contains the zero-based index of the current link in the nav bar.
        /// </summary>
        public int CurrentLink;
        /// <summary>
        /// Contains the remote links.
        /// </summary>
        public IEnumerable<Link> RemoteLinks;
        /// <summary>
        /// Contains the archive list.
        /// </summary>
        public IEnumerable<ArchiveEntry> Archive;
        /// <summary>
        /// Contains whether to hide the sidebar.
        /// </summary>
        public bool HideSidebar;
    }
}