﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// Contains static helper methods.
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Gets a 3-4 letter string for the name of the month.
        /// </summary>
        /// <param name="month">The 1-based index of the month.</param>
        /// <returns>The name of the month.</returns>
        public static string GetMonth(int month)
        {
            switch (month)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "Jun";
                case 7: return "July";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "";
            }
        }
    }
}