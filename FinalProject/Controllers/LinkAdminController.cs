﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    /// <summary>
    /// The controller that manages requests to /LinkAdmin
    /// 
    /// This page lists the current links and allows modifications to them.
    /// </summary>
    public class LinkAdminController : Controller
    {
        /// <summary>
        /// Displays the current links. GET /LinkAdmin
        /// </summary>
        public ActionResult Index()
        {
            using (var con = new DataContext())
            {
                return View(con.Links);
            }
        }

        /// <summary>
        /// Shows the form for a new link.  GET /LinkAdmin/New
        /// </summary>
        public ActionResult New()
        {
            return View("Single", new Link());
        }

        /// <summary>
        /// Shows the form for editing an link.  GET /LinkAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the link to edit.</param>
        public ActionResult Edit(int id)
        {
            using (var con = new DataContext())
            {
                var model = con.Links.Where(a => a.Id == id).FirstOrDefault();
                if (model == null)
                    return RedirectToAction("Index");
                else
                    return View("Single", model);
            }
        }
        /// <summary>
        /// Post location for the form for new or editing links. POST /LinkAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the link to edit.</param>
        /// <param name="cancel">Whether the action was canceled.</param>
        /// <param name="text">The text of the link.</param>
        /// <param name="href">The location of the link.</param>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, bool cancel, string text, string href)
        {
            if (!cancel)
            {
                using (var con = new DataContext())
                {
                    Link link;
                    if (id == -1)
                    {
                        link = new Link();
                    }
                    else
                    {
                        link = con.Links.Where(a => a.Id == id).First();
                    }

                    link.Text = text;
                    link.Href = href;

                    con.UpdateLink(link);
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Deletes the given link.  POST /LinkAdmin/Delete/{id}
        /// </summary>
        /// <param name="id">The id of the link to delete.</param>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var con = new DataContext())
            {
                con.DeleteLink(id);
            }
            return RedirectToAction("Index");
        }
    }
}
