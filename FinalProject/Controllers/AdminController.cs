﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// The controller that manages requests to /Admin.
    /// 
    /// This page displays links to the other admin pages.
    /// </summary>
    public class AdminController : Controller
    {
        /// <summary>
        /// Displays the links for the admin page. GET /Admin
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }
    }
}
