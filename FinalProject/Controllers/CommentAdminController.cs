﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Handles requests to /CommentAdmin
    /// 
    /// This page lists the current comments and allows modifications to them.
    /// </summary>
    public class CommentAdminController : Controller
    {
        /// <summary>
        /// Displays the current authors. GET /CommentAdmin
        /// </summary>
        public ActionResult Index()
        {
            using (var con = new DataContext())
            {
                ViewBag.CanAdd = (con.Posts.Length > 0);
                return View(con.Comments);
            }
        }

        /// <summary>
        /// Shows the form for a new comment.  GET /CommentAdmin/New
        /// </summary>
        public ActionResult New()
        {
            using (var con = new DataContext())
                return View("Single", new CommentModel(new Comment(), con.PostNames));
        }
        /// <summary>
        /// Shows the form for editing an comment.  GET /CommentAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the comment to edit.</param>
        public ActionResult Edit(int id)
        {
            using (var con = new DataContext())
            {
                var model = con.Comments.Where(a => a.Id == id).FirstOrDefault();
                if (model == null)
                    return RedirectToAction("Index");
                else
                    return View("Single", new CommentModel(model, con.PostNames));
            }
        }
        /// <summary>
        /// Post location for the form for new or editing comments. POST /CommentAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the comment to edit.</param>
        /// <param name="cancel">Whether the action was canceled.</param>
        /// <param name="user">The name of the user.</param>
        /// <param name="post">The post the comment is for.</param>
        /// <param name="text">The contents of the comment.</param>
        /// <param name="date">The date and time of the comment.</param>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, bool cancel, string user, string post, string text, string date)
        {
            if (!cancel)
            {
                using (var con = new DataContext())
                {
                    Comment comment;
                    if (id == -1)
                    {
                        comment = new Comment();
                        comment.Post = post;
                        comment.User = user;
                    }
                    else
                    {
                        comment = con.Comments.Where(a => a.Id == id).First();
                    }

                    comment.Text = text;
                    comment.Date = DateTime.Parse(date);

                    con.UpdateComment(comment);
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Deletes the given comment.  POST /CommentAdmin/Delete/{id}
        /// </summary>
        /// <param name="id">The id of the comment to delete.</param>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var con = new DataContext())
            {
                con.DeleteComment(id);
            }
            return RedirectToAction("Index");
        }
    }
}
