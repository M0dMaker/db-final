﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    /// <summary>
    /// This displays the views with the blog, the URL's have the following form:
    /// 
    /// /Blog/{year}/{month}/{name}
    /// -OR-
    /// /Blog/{year}/{month}?offset={offset}
    /// -OR-
    /// /Blog/{year}?offset={offset}
    /// </summary>
    public class BlogController : BaseController
    {
        /// <summary>
        /// Handles requests to the blog.  GET /Blog/*
        /// </summary>
        /// <param name="year">The year of the archive.</param>
        /// <param name="month">The month of the archive.</param>
        /// <param name="name">The name of the post.</param>
        /// <param name="offset">The offset in the list.</param>
        public ActionResult Index(string year, string month, string name, int? offset)
        {
            if (offset < 0)
                offset = 0;
            if (!string.IsNullOrEmpty(year))
            {
                int _year;
                if (!int.TryParse(year, out _year))
                    return NotFound();

                if (!string.IsNullOrEmpty(month))
                {
                    int _month;
                    if (!int.TryParse(month, out _month))
                        return NotFound();
                    else
                    {
                        if (string.IsNullOrEmpty(name))
                            return View(new BlogIndexModel(entities.GetPosts(_year, _month), offset ?? 0, _year, _month));
                        else
                        {
                            Post post = entities.Posts.Where(p => p.Title == name).First();
                            return View("Single", new BlogSingleModel(post, entities.GetComments(post.Id)));
                        }
                    }
                }
                else
                    return View(new BlogIndexModel(entities.GetPosts(_year, null), offset ?? 0, _year, null));
            }

            return View(new BlogIndexModel(entities.Posts, offset ?? 0, null, null));
        }
    }
}
