﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    /// <summary>
    /// The controller that manages requests to /AuthorAdmin
    /// 
    /// This page lists the current authors and allows modifications to them.
    /// </summary>
    public class AuthorAdminController : Controller
    {
        /// <summary>
        /// Displays the current authors. GET /AuhorAdmin
        /// </summary>
        public ActionResult Index()
        {
            using (var con = new DataContext())
            {
                return View(con.Authors);
            }
        }

        /// <summary>
        /// Shows the form for a new author.  GET /AuthorAdmin/New
        /// </summary>
        public ActionResult New()
        {
            return View("Single", new Author());
        }
        /// <summary>
        /// Shows the form for editing an author.  GET /AuthorAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the author to edit.</param>
        public ActionResult Edit(int id)
        {
            using (var con = new DataContext())
            {
                var model = con.Authors.Where(a => a.Id == id).FirstOrDefault();
                if (model == null)
                    return RedirectToAction("Index");
                else
                    return View("Single", model);
            }
        }
        /// <summary>
        /// Post location for the form for new or editing authors. POST /AuthorAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the author to edit.</param>
        /// <param name="cancel">Whether the action was canceled.</param>
        /// <param name="name">The name of the author.</param>
        /// <param name="email">The email of the author.</param>
        /// <param name="join">The join date of the author.</param>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, bool cancel, string name, string email, string join)
        {
            if (!cancel)
            {
                using (var con = new DataContext())
                {
                    Author author;
                    if (id == -1)
                    {
                        author = new Author();
                    }
                    else
                    {
                        author = con.Authors.Where(a => a.Id == id).First();
                    }

                    author.Name = name;
                    author.Email = email;
                    author.JoinDate = DateTime.Parse(join);

                    con.UpdateAuthor(author);
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Deletes the given author.  POST /AuthorAdmin/Delete/{id}
        /// </summary>
        /// <param name="id">The id of the author to delete.</param>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var con = new DataContext())
            {
                con.DeleteAuthor(id);
            }
            return RedirectToAction("Index");
        }
    }
}
