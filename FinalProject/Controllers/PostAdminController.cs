﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    /// <summary>
    /// The controller that manages requests to /PostAdmin
    /// 
    /// This page lists the current posts and allows modifications to them.
    /// </summary>
    public class PostAdminController : Controller
    {
        /// <summary>
        /// Displays the current posts. GET /PostAdmin
        /// </summary>
        public ActionResult Index()
        {
            using (var con = new DataContext())
            {
                ViewBag.CanAdd = (con.AuthorNames.Length > 0);
                return View(con.Posts);
            }
        }

        /// <summary>
        /// Shows the form for a new post.  GET /PostAdmin/New
        /// </summary>
        public ActionResult New()
        {
            using (var con = new DataContext())
                return View("Single", new PostModel(new Post(), new Comment[0], con.AuthorNames));
        }
        /// <summary>
        /// Shows the form for editing an post.  GET /PostAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the post to edit.</param>
        public ActionResult Edit(int id)
        {
            using (var con = new DataContext())
            {
                var model = con.Posts.Where(a => a.Id == id).FirstOrDefault();
                if (model == null)
                    return RedirectToAction("Index");
                else
                    return View("Single", new PostModel(model, con.GetComments(id), con.AuthorNames));
            }
        }

        /// <summary>
        /// Post location for the form for new or editing posts. POST /PostAdmin/Edit/{id}
        /// </summary>
        /// <param name="id">The id of the post to edit.</param>
        /// <param name="cancel">Whether the action was canceled.</param>
        /// <param name="title">The title of the post.</param>
        /// <param name="text">The contents of the post.</param>
        /// <param name="date">The date of the post.</param>
        /// <param name="category">The category of the post.</param>
        /// <param name="author">The author of the post.</param>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, bool cancel, string title, string text,
            string date, string category, string author)
        {
            if (!cancel)
            {
                using (var con = new DataContext())
                {
                    Post post;
                    if (id == -1)
                    {
                        post = new Post();
                        post.Author = author;
                    }
                    else
                    {
                        post = con.Posts.Where(p => p.Id == id).First();
                    }

                    post.Title = title;
                    post.Text = text;
                    post.Category = category;
                    post.Date = DateTime.Parse(date);

                    con.UpdatePost(post);
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Deletes the given post.  POST /PostAdmin/Delete/{id}
        /// </summary>
        /// <param name="id">The id of the post to delete.</param>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var con = new DataContext())
            {
                con.DeletePost(id);
            }
            return RedirectToAction("Index");
        }
    }
}
