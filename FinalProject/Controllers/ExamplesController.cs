﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// This displays the /Examples page.
    /// </summary>
    public class ExamplesController : BaseController
    {
        /// <summary>
        /// Display the exmaple page.  GET /Examples
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}
