﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    /// <summary>
    /// The base class for all the normal page controllers, the Admin controllers
    /// do not have this base class.  This modifies the incomming requests so they
    /// all have the correct model data since it is all the same.  This also
    /// gives a model if none is given.
    /// </summary>
    public class BaseController : Controller
    {
        /// <summary>
        /// Contains the data context for the controller.  Used to communicate 
        /// with the SQL server.
        /// </summary>
        protected DataContext entities = new DataContext();

        /// <summary>
        /// Called when the controller wants to create a view.
        /// </summary>
        /// <param name="viewName">The name of the view to create.</param>
        /// <param name="masterName">The name of the master view.</param>
        /// <param name="model">The model object to use.</param>
        protected override ViewResult View(string viewName, string masterName, object model)
        {
            MasterModel master = model as MasterModel ?? new MasterModel();
            SetupModel(master, GetNavIndex(this));

            return base.View(viewName, masterName, master);
        }

        /// <summary>
        /// Creates an action result when something is not found.
        /// </summary>
        protected ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View("Errors/404");
        }

        /// <summary>
        /// Sets up the master model for the view.
        /// </summary>
        /// <param name="model">The model to setup.</param>
        /// <param name="index">The index in the nav pane.</param>
        void SetupModel(MasterModel model, int index)
        {
            model.CurrentLink = index;
            model.RemoteLinks = entities.Links;
            model.Archive = entities.Archives;
        }

        /// <summary>
        /// Gets the index in the Nav pane.
        /// </summary>
        /// <param name="controller">The current controller.</param>
        /// <returns>The index of the selected page.</returns>
        static int GetNavIndex(ControllerBase controller)
        {
            if (controller is HomeController)
                return 0;
            else if (controller is ExamplesController)
                return 1;
            else if (controller is BlogController)
                return 2;
            else
                return -1;
        }
    }
}
