﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// This manages requests to the homepage.
    /// </summary>
    public class HomeController : BaseController
    {
        /// <summary>
        /// Displays the homepage.  GET /
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }
    }
}
