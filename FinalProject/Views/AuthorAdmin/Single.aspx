﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.Author>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Model.Id == -1 ? "New" : "Edit" %> Author</h2>
    <% using (Html.BeginForm("Edit", "AuthorAdmin", FormMethod.Post, new { id = "form" })) { %>
        <input type="hidden" value="false" name="cancel" id="cancel" />
        <input type="hidden" value="<%: Model.Id %>" name="id" />
        <p><label for="name">Name:</label> 
            <input type="text" name="name" id="name" value="<%: Model.Name %>" /></p>
        <p><label for="email">Email:</label> 
            <input type="text" name="email" id="email" value="<%: Model.Email %>" /></p>
        <p><label for="join">Join Date:</label> 
            <input type="text" name="join" id="join" value="<%: Model.JoinDate %>" /></p>

        <p><input type="submit" /> <input type="reset" value="Cancel" onclick="Clicked()" /></p>
    <% } %>
    
    <script type="text/javascript">
        function Clicked() {
            document.getElementById("cancel").value = "true";
            document.getElementById("form").submit();
        }
    </script>
</asp:Content>
