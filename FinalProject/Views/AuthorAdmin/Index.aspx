﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Author[]>" %>
<%@Import Namespace="FinalProject.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Authors</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Join Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <% foreach (var item in Model) { %>
            <tr>
                <td><%: item.Id %></td>
                <td><%: item.Name %></td>
                <td><%: item.Email %></td>
                <td><%: item.JoinDate %></td>
                <td><%: Html.ActionLink("Edit", "Edit", "AuthorAdmin", new { id = item.Id }, null) %></td>
                <% if (item.CanDelete) { %>
                    <td><a href="#" onclick="OnDelete(<%: item.Id %>); return false;">Delete</a></td>
                <% } else { %>
                    <td title="Cannot delete while this author has a post.">Delete</td>
                <% } %>
            </tr>
        <% } %>
    </table>

    <% using (Html.BeginForm("New", "AuthorAdmin")) { %>
        <p><input type="submit" value="New" /></p>
    <% } %>
    
    <% using (Html.BeginForm("Delete", "AuthorAdmin", null, FormMethod.Post, new { id = "delete-form" }))  {  %>
        <input type="hidden" name="id" value="-1" id="delete-id" />
    <% } %>
    
    <script type="text/javascript">
        function OnDelete(id) {
            if (confirm("Are you sure you want to delete the selected author?")) {
                document.getElementById("delete-id").value = id;
                document.getElementById("delete-form").submit();
            }
        }
    </script>
</asp:Content>
