﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Comment[]>" %>
<%@ Import Namespace="FinalProject.Models" %>

<% if (Model.Length > 0) { %>

    <section class="comments">
        <h2><%: Model.Length %> Comment<%: Model.Length == 1 ? "" : "s" %></h2>

        <% foreach (var comment in Model) { %>

            <article class="comment">
                <div>
				    <h3><%: comment.User %> says:</h3>
				    <div class="comment-date"><%: comment.Date %></div>

				    <%= comment.Text %>
			    </div>
            </article>

        <% } %>

    </section>

<% } %>

<section class="reply">
    <h2>Leave a Reply</h2>
    <article id="reply">
        <p class="disabled">This feature is currently disabled.</p>
    </article>
</section>