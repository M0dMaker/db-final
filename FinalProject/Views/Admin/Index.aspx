﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Select an Option</h2>

    <ul>
        <li><%: Html.ActionLink("Authors", "Index", "AuthorAdmin") %></li>
        <li><%: Html.ActionLink("Comments", "Index", "CommentAdmin") %></li>
        <li><%: Html.ActionLink("Posts", "Index", "PostAdmin") %></li>
        <li><%: Html.ActionLink("Links", "Index", "LinkAdmin") %></li>
    </ul>

    <p><%: Html.ActionLink("Home", "Index", "Home") %></p>
</asp:Content>
