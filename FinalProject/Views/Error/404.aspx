﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <article>
        <h1>404 Page Not Found</h1>
        <p>
            The resource you are looking for might have been removed, had its name changed, or 
            is temporarily unavailable. This is most likely a temporary thing, it will be 
            fixed shortly. A report of the error was sent to the server. Thank you for your patience.
        </p>
    </article>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
