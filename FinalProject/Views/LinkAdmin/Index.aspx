﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.Link[]>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Links</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>Text</th>
            <th>Href</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <% foreach (var item in Model) { %>
            <tr>
                <td><%: item.Id %></td>
                <td><%: item.Text %></td>
                <td><%: item.Href %></td>
                <td><%: Html.ActionLink("Edit", "Edit", "LinkAdmin", new { id = item.Id }, null) %></td>
                <td><a href="#" onclick="OnDelete(<%: item.Id %>); return false;">Delete</a></td>
            </tr>
        <% } %>
    </table>

    <% using (Html.BeginForm("New", "LinkAdmin")) { %>
        <p><input type="submit" value="New" /></p>
    <% } %>
    
    <% using (Html.BeginForm("Delete", "LinkAdmin", null, FormMethod.Post, new { id = "delete-form" })){  %>
        <input type="hidden" name="id" value="-1" id="delete-id" />
    <% } %>
    
    <script type="text/javascript">
        function OnDelete(id) {
            if (confirm("Are you sure you want to delete the selected Link?")) {
                document.getElementById("delete-id").value = id;
                document.getElementById("delete-form").submit();
            }
        }
    </script>
</asp:Content>
