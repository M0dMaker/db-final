﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.Link>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Model.Id == -1 ? "New" : "Edit" %> Link</h2>
    <% using (Html.BeginForm("Edit", "LinkAdmin", FormMethod.Post, new { id="form" })) { %>
        <input type="hidden" value="false" name="cancel" id="cancel" />
        <input type="hidden" value="<%: Model.Id %>" name="id" />
        <p><label for="text">Text:</label> 
            <input type="text" name="text" id="text" value="<%: Model.Text %>" /></p>
        <p><label for="href">Href:</label> 
            <input type="text" name="href" id="href" value="<%: Model.Href %>" /></p>

        <p><input type="submit" /> <input type="reset" value="Cancel" onclick="Clicked()" /></p>
    <% } %>
    
    <script type="text/javascript">
        function Clicked() {
            document.getElementById("cancel").value = "true";
            document.getElementById("form").submit();
        }
    </script>
</asp:Content>
