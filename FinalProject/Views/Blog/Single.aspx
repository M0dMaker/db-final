﻿<%@ Page Title="Blog - Jacob Trimble" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<BlogSingleModel>" %>
<%@ Import Namespace="FinalProject.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <article>
        <div class="post-date">
		    <span class="month"><%: Helpers.GetMonth(Model.Post.Date.Month) %></span>
		    <span class="day"><%: Model.Post.Date.Day %></span>
            <span class="year"><%: Model.Post.Date.Year %></span>
        </div>
        <div class="post-content">
            <h1>
                <%: Html.ActionLink(Model.Post.Title, "Index", "Blog", 
                new { year = Model.Post.Date.Year, month = Model.Post.Date.Month, name = Model.Post.Title }, 
                new { title = "Permalink to here" }) %>
            </h1>
            <div class="post-meta">
                <ul class="categories">
                    <li><%: Model.Post.Category %></li>
                </ul>
            </div>

            <%= Model.Post.Text %>

            <div class="comment-link">
                <%: Html.ActionLink(Model.Post.Comments, "Index", "Blog", null, null, "reply", 
                new { year = Model.Post.Date.Year, month = Model.Post.Date.Month, name = Model.Post.Title }, null) %>
            </div>
        </div>
    </article>
    
    <% Html.RenderPartial("Comments", Model.Comments); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
