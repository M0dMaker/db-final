﻿<%@ Page Title="Blog - Jacob Trimble" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<BlogIndexModel>" %>
<%@ Import Namespace="FinalProject.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Model.Year != null) { %>
        <% if (Model.Month != null) { %>
            <h1 class="category">Monthly Archive <span><%: Model.Month %> <%: Model.Year %></span></h1>
        <% } else { %>
            <h1 class="category">Yearly Archive <span><%: Model.Year %></span></h1>
        <% } %>
    <% } %>

    <% if (Model.Posts.Length == 0) {  %>

            <article>
                <h2><%: Html.ActionLink("Blog", "Index", "Blog") /* keep the current arguments */ %></h2>

                <p>There are currently no entries for this category. Please check back later to see if there are any.</p>
                <p><%: Html.ActionLink("Back to Blog", "Index", "Blog", new { name = UrlParameter.Optional, year = UrlParameter.Optional , month = UrlParameter.Optional }, null) %></p>
            </article>

    <% } else { %>
        <% if (Model.NextOffset >= 0 || Model.PrevOffset >= 0) {  %>
            <div class="page-nav clear-fix">
                <% if (Model.PrevOffset >= 0) { %>
                    <div class="left">
                        <%: Html.ActionLink("<< Previous 5 Posts", "Index", "Blog", 
                        new { month = Model.RawMonth, year = Model.Year, name = "", offset = Model.PrevOffset }, null) %>
                    </div>
                <% } %>
                <% if (Model.NextOffset >= 0) { %>
                    <div class="right">
                        <%: Html.ActionLink("Next 5 Posts >>", "Index", "Blog", 
                        new { month = Model.RawMonth, year = Model.Year, name = "", offset = Model.NextOffset }, null) %>
                    </div>
                <% } %>
            </div>
        <% } %>

        <% foreach (var item in Model.Posts) { %>
            
            <article>
                <div class="post-date">
		            <span class="month"><%: Helpers.GetMonth(item.Date.Month) %></span>
		            <span class="day"><%: item.Date.Day %></span>
		            <span class="year"><%: item.Date.Year %></span>
                </div>
                <div class="post-content">
                    <h2>
                        <%: Html.ActionLink(item.Title, "Index", "Blog", new { name = item.Title, month = item.Date.Month, year = item.Date.Year }, null)%>
                    </h2>
                    <div class="post-meta">
                        <ul class="categories">
                            <li><%: item.Category %></li>
                        </ul>
                    </div>

                    <%= item.Text %>
                    
                    <div class="comment-link">
                        <%: Html.ActionLink(item.Comments, "Index", "Blog", null, null, "reply", 
                            new { name = item.Title, month = item.Date.Month, year = item.Date.Year }, null) %>
                    </div>
                </div>
            </article>

        <% } %>
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
