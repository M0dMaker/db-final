﻿<%@ Page Title="Jacob Trimble" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <article>
        <h1><%: Html.ActionLink("Jacob Trimble", "Index", new { }, new { title = "Permalink to here" }) %></h1>
		<p>
            This site is my final project for TCSS 445B.  This site uses dynamic data for the blog and the sidebar
            links.  The dynamic data is loaded from a local database file.  This site uses ASP.NET MVC 4 for the 
            web service and uses SQL Server for the database.  The source code for this site can be downloaded from 
            <a target="_blank" href="https://bitbucket.org/M0dMaker/db-final">BitBucket</a>.  
		</p>
        <p>
            Running the code requires Visual Studio 2012 and ASP.NET MVC 4.  However, the current site is a working 
            version of it.  All the elements of the blog as well as the sidebar are loaded from the database.  
            Writing to the database can be done from the <%: Html.ActionLink("Admin panel", "Index", "Admin") %>.  
            It allows you to write to the database and see the changes to the site.
        </p>
        <p>
            The source code is mainly separated into the model, view and controllers (MVC).  The view is the web
            pages that are displayed.  The controllers are what code is called when a page is requested, it sets up
            the data and passes it to the view.  The model simply stores the data and is used to communicate between
            the controller and the view.  The main class that manages the communication with the database is the 
            <a target="_blank" href="https://bitbucket.org/M0dMaker/db-final/src/7414f25abe762e91b6e9173d0ccd7083b1510273/FinalProject/Models/DataContext.cs?at=master">DataContext</a> 
            in the Models folder.
        </p>
    </article>
</asp:Content>