﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.Post[]>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Posts</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <% foreach (var item in Model) { %>
            <tr>
                <td><%: item.Id %></td>
                <td><%: item.Title %></td>
                <td><%: item.Author %></td>
                <td><%: item.Date %></td>
                <td><%: Html.ActionLink("Edit", "Edit", "PostAdmin", new { id = item.Id }, null) %></td>
                <td><a href="#" onclick="OnDelete(<%: item.Id %>); return false;">Delete</a></td>
            </tr>
        <% } %>
    </table>

    <% using (Html.BeginForm("New", "PostAdmin")) { %>
        <p><input type="submit" value="New" <%= ViewBag.CanAdd ? "" : "disabled=\"disabled\" title=\"Cannot add a post when there are no authors.\"" %> /></p>
    <% } %>
    
    <% using (Html.BeginForm("Delete", "PostAdmin", null, FormMethod.Post, new { id = "delete-form" })) {  %>
        <input type="hidden" name="id" value="-1" id="delete-id" />
    <% } %>
    
    <script type="text/javascript">
        function OnDelete(id) {
            if (confirm("Are you sure you want to delete the selected Post?")) {
                document.getElementById("delete-id").value = id;
                document.getElementById("delete-form").submit();
            }
        }
    </script>
</asp:Content>
