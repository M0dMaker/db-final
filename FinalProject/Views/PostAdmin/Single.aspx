﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.PostModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Model.Post.Id == -1 ? "New" : "Edit" %> Post</h2>
    <% using (Html.BeginForm("Edit", "PostAdmin", FormMethod.Post, new { id="form" })) { %>
        <input type="hidden" value="false" name="cancel" id="cancel" />
        <input type="hidden" value="<%: Model.Post.Id %>" name="id" />
        <p><label for="title">Title:</label> 
            <input type="text" name="title" id="title" value="<%: Model.Post.Title %>" /></p>
        <p><label for="category">Category:</label> 
            <input type="text" name="category" id="category" value="<%: Model.Post.Category %>" /></p>
        <p><label for="post">Post:</label> 
            <%if (Model.Post.Author == null) { %>
            <select name="author" id="author">
            <% } else { %>
            <select name="author" id="Select1"disabled="disabled" title="Cannot modify after created.">
            <% } %>
                <% foreach(var item in Model.Authors) { %>
                    <option <%: Model.Post.Author == item ? "selected=\"selected\"" : "" %>><%: item %></option>
                <% } %>
            </select>
        </p>
        <p><label for="date">Date:</label> 
            <input type="text" name="date" id="date" value="<%: Model.Post.Date %>" /></p>
        <p>
            <label for="text">Content:</label> 
            <textarea name="text" id="text" rows="20"><%: Model.Post.Text %></textarea>
        </p>

        <% if (Model.Post.Id != -1 && Model.Comments.Length > 0) { %>
            <h2>Comments</h2>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Author</th>
                    <th>Date</th>
                    <th>Text</th>
                </tr>

                <% foreach (var item in Model.Comments) { %>
                    <tr>
                        <td><%: item.Id %></td>
                        <td><%: item.User %></td>
                        <td><%: item.Date %></td>
                        <td><%: item.Text %></td>
                    </tr>
                <% } %>
            </table>
        <% } %>

        <p><input type="submit" /> <input type="reset" value="Cancel" onclick="Clicked()" /></p>
    <% } %>

    
    <script type="text/javascript">
        function Clicked() {
            document.getElementById("cancel").value = "true";
            document.getElementById("form").submit();
        }
    </script>
</asp:Content>
