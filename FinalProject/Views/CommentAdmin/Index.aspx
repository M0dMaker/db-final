﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.Comment[]>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Comments</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>Post</th>
            <th>User</th>
            <th>Text</th>
            <th>Date</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <% foreach (var item in Model) { %>
            <tr>
                <td><%: item.Id %></td>
                <td><%: item.Post %></td>
                <td><%: item.User %></td>
                <td><%: item.Text %></td>
                <td><%: item.Date %></td>
                <td><%: Html.ActionLink("Edit", "Edit", "CommentAdmin", new { id = item.Id }, null) %></td>
                <td><a href="#" onclick="OnDelete(<%: item.Id %>); return false;">Delete</a></td>
            </tr>
        <% } %>
    </table>

    <% using (Html.BeginForm("New", "CommentAdmin")) { %>
        <p><input type="submit" value="New" <%= ViewBag.CanAdd ? "" : "disabled=\"disabled\" title=\"Cannot add a comment when there are no posts.\"" %> /></p>
    <% } %>
    
    <% using (Html.BeginForm("Delete", "CommentAdmin", null, FormMethod.Post, new { id = "delete-form" })) {  %>
        <input type="hidden" name="id" value="-1" id="delete-id" />
    <% } %>
    
    <script type="text/javascript">
        function OnDelete(id) {
            if (confirm("Are you sure you want to delete the selected Comment?")) {
                document.getElementById("delete-id").value = id;
                document.getElementById("delete-form").submit();
            }
        }
    </script>
</asp:Content>
