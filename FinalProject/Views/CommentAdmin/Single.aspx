﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<FinalProject.Models.CommentModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Model.Comment.Id == -1 ? "New" : "Edit" %> Comment</h2>
    <% using (Html.BeginForm("Edit", "CommentAdmin", FormMethod.Post, new { id="form" })) { %>
        <input type="hidden" value="false" name="cancel" id="cancel" />
        <input type="hidden" value="<%: Model.Comment.Id %>" name="id" />
        <p><label for="user">User:</label> 
            <input type="text" name="user" id="user" value="<%: Model.Comment.User %>" <%= Model.Comment.Id == -1 ? "" : "disabled=\"disabled\"" %> /></p>
        <p><label for="post">Post:</label> 
            <%if (Model.Comment.Post == null) { %>
            <select name="post" id="post">
            <% } else { %>
            <select name="post" id="Select1"disabled="disabled" title="Cannot modify after created.">
            <% } %>
                <% foreach(var item in Model.Posts) { %>
                    <option <%: Model.Comment.Post == item ? "selected=\"selected\"" : "" %>><%: item %></option>
                <% } %>
            </select>
        </p>
        <p><label for="date">Date:</label> 
            <input type="text" name="date" id="date" value="<%: Model.Comment.Date %>" /></p>
        <p>
            <label for="text">Content:</label> 
            <textarea name="text" id="text" rows="20"><%: Model.Comment.Text %></textarea>
        </p>

        <p><input type="submit" /> <input type="reset" value="Cancel" onclick="Clicked()" /></p>
    <% } %>
    
    <script type="text/javascript">
        function Clicked() {
            document.getElementById("cancel").value = "true";
            document.getElementById("form").submit();
        }
    </script>
</asp:Content>
