﻿<%@ Page Title="Examples - Jacob Trimble" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <article>
        <h1><%: Html.ActionLink("Examples", "Index", new { }, new { title = "Permalink to here" }) %></h1>
		<p>
            This page shows how to use the <%: Html.ActionLink("Admin panel", "Index", "Admin") %> and some
            examples of changes made to the database.
		</p>
        <ul>
            <li><a href="#main">Main Page</a></li>
            <li><a href="#links">Links Page</a></li>
            <li><a href="#authors">Authors Page</a></li>
            <li><a href="#comments">Comments Page</a></li>
            <li><a href="#posts">Posts Page</a></li>
        </ul>

        <h2 id="main">Main Page</h2>
        <img src="/Content/Images/Main.png" alt="Main Page" />
        <p>
            This is the main admin page.  You can view each of the main tables by clicking on each of the links.
            You can click the 'Administration' title to return to this page.  You can click the 'Home' link
            to return to the main homepage.
        </p>

        <h2 id="links">Links Page</h2>
        <img src="/Content/Images/Links.png" alt="Links Page" />
        <p>
            This is the links page, this contains the links in the sidebar.  To add, edit, or remove an item, click
            the appropriate button or link.  The delete will ask for a confirmation before deleting.  The add or
            edit will take you to the following page
        </p>
        <img src="/Content/Images/LinksNew.png" alt="Links New Page" />
        <p>
            Once you enter the text of the link in the first box and the URL in the second box, click Submit to add
            a new Link, now it should display the following
        </p>
        <img src="/Content/Images/LinksEdited.png" alt="Links Edited Page" />

        <h2 id="authors">Authors Page</h2>
        <img src="/Content/Images/Authors.png" alt="Authors Page" />
        <p>
            This is the authors page, this defines the authors of posts in the blog.  People must exist in here
            before a post can be made.  A link to delete an author will be disabled if an author has a post made
            since the post must be deleted first.  Editing and adding a new author is the same as for the 
            <a href="#links">links</a>.
        </p>

        <h2 id="comments">Comments Page</h2>
        <img src="/Content/Images/Comments.png" alt="Comments Page" />
        <p>
            This is the comments page, this has the comments for blog posts.  A post must exist before a comment can
            be made.  Once a comment is made, its post cannot be changed.  You select the post that a new comment will
            be for from a drop-down list.
        </p>

        <h2 id="posts">Posts Page</h2>
        <img src="/Content/Images/Posts.png" alt="Posts Page" />
        <p>
            This is the posts page, this has the posts for the blog.  An author must exist before a post can be made.
            Like the comments page, you select an author from a drop-down list and once selected, an author cannot
            be changed.  If a post has comments, the comments are displayed at the bottom of the edit page.  If you
            delete a post, the comments for the post are deleted also.
        </p>
        <img src="/Content/Images/PostsEdit.png" alt="Posts Page" />
    </article>
</asp:Content>
